<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->insert([
            [
                'name' =>  'Admin GRTech',
                'email' => 'admin@grtech.com.my',
                'password' => Hash::make('password'),
            ],
            [
                'name' =>  'User GRTech',
                'email' => 'user@grtech.com.my',
                'password' => Hash::make('password'),
            ]
        ]);
    }
}
