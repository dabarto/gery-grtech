<?php

namespace App\Http\Controllers\Employee;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Company;
use App\Http\Requests\EmployeeRequest;
use App\Mail\NewEmployee;
use Illuminate\Support\Facades\Mail;

class EmployeeController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $companies = Company::get();
        return view('employee.index', compact('companies'));
    }

    public function store(EmployeeRequest $request)
    {

        $data = new Employee;
        $data->first_name = $request->first_name;
        $data->last_name = $request->last_name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->company_id =   $request->company_id;
        $data->save();

        Mail::to($data->company->email)->send(new NewEmployee($data->id));
        return redirect("employee")->with("message", "success!!");;
    }

    public function update(EmployeeRequest $request)
    {

        $data =  Employee::find($request->id);
        $data->first_name = $request->first_name;
        $data->last_name = $request->last_name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->company_id =   $request->company_id;
        $data->save();

        return redirect("employee")->with("message", "success!!");;
    }

    public function destroy($id)
    {
        Employee::destroy($id);
        return redirect("employee")->with("message", "delete success!!");;
    }
}
