<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Models\Company;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\CompanyUpdateRequest;

class CompanyController extends \App\Http\Controllers\Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('company.index');
    }

    public function store(CompanyRequest $request)
    {

        $imageName = time() . $request->logo->getClientOriginalName();

        $request->file('logo')->storeAs('public', $imageName);

        $data = new Company;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->website = $request->website;
        $data->logo =  $imageName;
        $data->save();

        return redirect("company")->with("message", "success!!");;
    }

    public function update(CompanyUpdateRequest $request)
    {

        $data =  Company::find($request->id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->website = $request->website;

        if (!empty($request->file('logo'))) {
            $request->validate([
                'logo' => 'required|mimes:jpeg,jpg,png|max:8000'
            ]);
            $imageName = time() . $request->logo->getClientOriginalName();
            $request->file('logo')->storeAs('public', $imageName);
            $data->logo =  $imageName;
        }

        $data->save();

        return redirect("company")->with("message", "success!!");;
    }

    public function destroy($id)
    {
        Company::destroy($id);
        return redirect("company")->with("message", "delete success!!");;
    }
}
