<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Company extends Model
{
    use SoftDeletes, LaravelVueDatatableTrait, HasFactory;

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dataTableColumns = [
        'id' => [
            'searchable' => false,
        ],
        'name' => [
            'searchable' => true,
        ],
        'email' => [
            'searchable' => true,
        ],
        'website' => [
            'searchable' => true,
        ],
        'logo' => [
            'searchable' => false,
        ]
    ];


    protected $fillable = [
        'name',
        'email',
        'website',
        'logo',
    ];

    public function getLogoAttribute($value)
    {
        return  asset('storage/' . $value);;
    }
}
