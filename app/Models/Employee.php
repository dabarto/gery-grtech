<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use JamesDordoy\LaravelVueDatatable\Traits\LaravelVueDatatableTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use SoftDeletes, LaravelVueDatatableTrait, HasFactory;

    protected $appends = ['full_name'];
    protected $dataTableColumns = [
        'id' => [
            'searchable' => false,
        ],
        'first_name' => [
            'searchable' => true,
        ],
        'last_name' => [
            'searchable' => true,
        ],
        'email' => [
            'searchable' => true,
        ],
        'phone' => [
            'searchable' => true,
        ],
        
    ];

    protected $dataTableRelationships = [
        "belongsTo" => [
            'company' => [
                "model" => \App\Models\Company::class,
                'foreign_key' => 'company_id',
                'columns' => [
                    'name' => [
                        'searchable' => true,
                        'orderable' => true,
                    ],
                ],
            ],
        ],
    ];


    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'company_id',
    ];

 

    public function company()
    {
        return $this->belongsTo(\App\Models\Company::class);
    }

    public function getFullNameAttribute()
    {
        return  $this->first_name.' '.$this->last_name;
    }
}
