<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes(['register' => false]);

Route::group(['prefix' => 'company', 'as' => 'company.',  'middleware' => ['auth','valid_user']], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'App\Http\Controllers\Company\CompanyController@index']);
    Route::post('/', ['as' => 'store', 'uses' => 'App\Http\Controllers\Company\CompanyController@store']);
    Route::put('/', ['as' => 'update', 'uses' => 'App\Http\Controllers\Company\CompanyController@update']);
    Route::delete('/{id}', ['as' => 'delete', 'uses' => 'App\Http\Controllers\Company\CompanyController@destroy']);
    Route::get('/api', ['as' => 'api.index', 'uses' => 'App\Http\Controllers\Company\CompanyAPIController@index']);
    Route::get('/api/list', ['as' => 'api.list', 'uses' => 'App\Http\Controllers\Company\CompanyAPIController@list']);
});

Route::group(['prefix' => 'employee', 'as' => 'employee.', 'middleware' => ['auth','valid_user']], function () {
    Route::get('/', ['as' => 'index', 'uses' => 'App\Http\Controllers\Employee\EmployeeController@index']);
    Route::post('/', ['as' => 'store', 'uses' => 'App\Http\Controllers\Employee\EmployeeController@store']);
    Route::put('/', ['as' => 'update', 'uses' => 'App\Http\Controllers\Employee\EmployeeController@update']);
    Route::delete('/{id}', ['as' => 'delete', 'uses' => 'App\Http\Controllers\Employee\EmployeeController@destroy']);
    Route::get('/api', ['as' => 'api.index', 'uses' => 'App\Http\Controllers\Employee\EmployeeAPIController@index']);
});
