/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('././bootstrap');

import Vue from 'vue'
import DataTable from 'laravel-vue-datatable';
import EditModalComponent from './vue/employee/EditModalComponent.vue';
import axios from 'axios'

Vue.use(DataTable);



const app = new Vue({
    el: '#app',
    data() {
        return {
            companies: [],
            columns: [
                {
                    label: 'ID',
                    name: 'id',
                    orderable: true,
                },
                {
                    label: 'Full Name',
                    name: 'full_name',
                    orderable: true,
                   
                },
                {
                    label: 'Email',
                    name: 'email',
                    orderable: true,
                },
                {
                    label: 'Phone',
                    name: 'phone',
                    orderable: true,
                },
                {
                    label: 'Company',
                    name: 'company.name',
                    columnName: 'company.name',
                    orderable: true,
                },
                {
                    label: 'Action',
                    name: 'Action',
                    orderable: false,
                    component: EditModalComponent,
                },
            ]
        }
    },
    mounted () {
        axios
          .get('/company/api/list')
          .then(response => (this.companies = response.data))
      }
});