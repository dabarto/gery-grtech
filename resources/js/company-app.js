/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('././bootstrap');

import Vue from 'vue'
import DataTable from 'laravel-vue-datatable';
import LogoComponent from './vue/company/LogoComponent.vue';
import URLComponent from './vue/company/URLComponent.vue';
import EditModalComponent from './vue/company/EditModalComponent.vue';


Vue.use(DataTable);



const app = new Vue({
    el: '#app',
    data() {
        return {
            columns: [
                {
                    label: 'ID',
                    name: 'id',
                    orderable: true,
                },
                {
                    label: 'Name',
                    name: 'name',
                    orderable: true,
                },
                {
                    label: 'Email',
                    name: 'email',
                    orderable: true,
                },
                {
                    label: 'Website',
                    name: 'website',
                    orderable: true,
                    component: URLComponent,
                },
                {
                    label: 'Logo',
                    name: 'logo',
                    orderable: false,
                    component: LogoComponent,
                },
                {
                    label: 'Action',
                    name: 'Action',
                    orderable: false,
                    component: EditModalComponent,
                },
            ]
        }
    }
});