<!-- need to remove -->
<li class="nav-item">
    <a href="{{ route('company.index') }}" class="nav-link {{ (request()->is('company')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-briefcase"></i>
        <p>Companies</p>
    </a>
    <a href="{{ route('employee.index') }}" class="nav-link {{ (request()->is('employee')) ? 'active' : '' }}" >
        <i class="nav-icon fas fa-user"></i>
        <p>Employees</p>
    </a>
</li>
