@extends('layouts.app')

@section('content')

<div id="app">
   
    @include('alert')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Employees</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Employees</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @include('employee.create')
                    </div>
                    <div class="card-body">
                        <data-table :columns="columns"  :companies="companies" url="{{url('employee/api')}}" order-dir="desc">
                        </data-table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('js/employee-app.js') }}"></script>
@endsection