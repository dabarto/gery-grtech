<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#create">
    Create
</button>

<!-- Modal -->
<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cerate New Employee</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form enctype="multipart/form-data" method="POST" action="{{url('employee')}}">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">First Name</label>
                        <input type="text" class="form-control" id="name" Name="first_name"
                            placeholder="Enter first name" value="{{ old('first_name') }}">
                    </div>
                    <div class="form-group">
                        <label for="name">Last Name</label>
                        <input type="text" class="form-control" id="name" Name="last_name" placeholder="Enter last name"
                            value="{{ old('last_name') }}">
                    </div>

                    <div class="form-group">
                        <label> Company</label>
                        <select class="form-control " name="company_id">
                            @foreach($companies as $company)
                            <option value="{{$company->id}}" @if($company->id == old('company_id')) selected @endif>{{$company->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" class="form-control" id="email" name="email"
                            placeholder="Enter email address" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone number</label>
                        <input type="text" class="form-control" id="phone" name="phone"
                            placeholder="Enter phone number" value="{{ old('phone') }}">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>