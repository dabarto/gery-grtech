@if ($errors->any())
<section class="content-header">
    <div class="container-fluid">
        <ul class="alert alert-danger">
            <button class="close" data-dismiss="alert">
                ×
            </button>

            @foreach ($errors->all() as $error)

            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</section>
@endif

@if (Session::has('message'))
<section class="content-header">
    <div class="container-fluid">
        <div class="alert  alert-info">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa-lg fa fa-exclamation"></i>
            <strong>{{ Session::get('message') }}</strong>
        </div>
    </div>
</section>

@endif