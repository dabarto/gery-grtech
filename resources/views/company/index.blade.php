@extends('layouts.app')

@section('content')

<div id="app">
   
    @include('alert')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Companies</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Companies</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        @include('company.create')
                    </div>
                    <div class="card-body">
                        <data-table :columns="columns" url="{{url('company/api')}}" order-dir="desc">
                        </data-table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ mix('js/company-app.js') }}"></script>
@endsection