@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        Home
        @if (Session::has('message'))
        <section class="content-header">
            <div class="container-fluid">
                <div class="alert  alert-info">
                    <button class="close" data-dismiss="alert">
                        ×
                    </button>
                    <i class="fa-fw fa-lg fa fa-exclamation"></i>
                    <strong>{{ Session::get('message') }}</strong>
                </div>
            </div>
        </section>
        
        @endif
    </div>
    
<script src="{{ mix('js/app.js') }}"></script>
@endsection
